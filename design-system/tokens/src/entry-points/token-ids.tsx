export {
  getCSSCustomProperty,
  getTokenId,
  getFullyQualifiedTokenId,
} from '../token-ids';
